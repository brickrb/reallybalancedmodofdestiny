-- === Policy Changes ===

-- Professional Army Upgrade Discount (Crystalline Cat)
UPDATE ModifierArguments SET Value =  '33' WHERE ModifierId = 'PROFESSIONAL_ARMY_UNITUPGRADEDISCOUNT' AND Name = 'Amount';

-- Unit-Related Policies (Crystalline Cat)
UPDATE ModifierArguments SET Value = '50' WHERE Name = 'Amount' AND ModifierId LIKE 'MARITIMIEINDUSTRIES_%_PRODUCTION'; -- [sic]
UPDATE ModifierArguments SET Value = '50' WHERE Name = 'Amount' AND ModifierId LIKE 'PRESSGANGS_%_PRODUCTION';
UPDATE ModifierArguments SET Value = '50' WHERE Name = 'Amount' AND ModifierId LIKE 'INTERNATIONALWATERS_%_PRODUCTION';
UPDATE ModifierArguments SET Value = '50' WHERE Name = 'Amount' AND ModifierId LIKE 'LIMES_%PRODUCTION';

UPDATE Policies     SET PrereqCivic = 'CIVIC_NATIONALISM'  WHERE PolicyType  = 'POLICY_LEVEE_EN_MASSE';
UPDATE Policies     SET PrereqCivic = 'CIVIC_MOBILIZATION' WHERE PolicyType  = 'POLICY_INTERNATIONAL_WATERS';

UPDATE ObsoletePolicies SET ObsoletePolicy = 'POLICY_GRANDE_ARMEE'         WHERE PolicyType = 'POLICY_AGOGE';
UPDATE ObsoletePolicies SET ObsoletePolicy = 'POLICY_MILITARY_FIRST'       WHERE PolicyType = 'POLICY_CHIVALRY';
UPDATE ObsoletePolicies SET ObsoletePolicy = 'POLICY_MILITARY_FIRST'       WHERE PolicyType = 'POLICY_FEUDAL_CONTRACT';
UPDATE ObsoletePolicies SET ObsoletePolicy = 'POLICY_MILITARY_FIRST'       WHERE PolicyType = 'POLICY_PRESS_GANGS';

DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_MARITIME_INDUSTRIES' AND ObsoletePolicy = 'POLICY_PRESS_GANGS' ;
DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_MANEUVER'            AND ObsoletePolicy = 'POLICY_CHIVALRY' ;
  -- These two had two obsoletion policies each, one of the the correct one already. Just needed to delete the other...

DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_BASTIONS';
DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_DISCIPLINE';
DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_GRANDE_ARMEE';
DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_LIMES';
DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_RETAINERS';

-- Economic Policies (Crystalline Cat)
UPDATE ModifierArguments SET Value = '-33' WHERE ModifierId = 'EXPROPRIATION_PLOTPURCHASECOST' AND Name = 'Amount';
UPDATE ModifierArguments SET Value = '-33' WHERE ModifierId = 'LANDSURVEYORS_PLOTPURCHASECOST' AND Name = 'Amount';
UPDATE ModifierArguments SET Value =  '50' WHERE ModifierId = 'ILKUM_BUILDERPRODUCTION'        AND Name = 'Amount';
UPDATE ModifierArguments SET Value =  '50' WHERE ModifierId = 'PUBLICWORKS_BUILDERPRODUCTION'  AND Name = 'Amount';

UPDATE ObsoletePolicies  SET ObsoletePolicy = 'POLICY_PUBLIC_WORKS' WHERE PolicyType = 'POLICY_ILKUM';

DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_CORVEE';
DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_GOTHIC_ARCHITECTURE';
DELETE FROM ObsoletePolicies WHERE PolicyType = 'POLICY_URBAN_PLANNING';

/*
-- Policy Finishers (Suboptimal)

INSERT INTO PolicyModifiers                                   VALUES('POLICY_RATIONALISM', 'RATIONALISM_YIELD_MULT');
INSERT INTO Modifiers         (ModifierID, ModifierType)      VALUES('RATIONALISM_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('RATIONALISM_YIELD_MULT', 'YieldType', 'ARGTYPE_IDENTITY', 'YIELD_SCIENCE');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('RATIONALISM_YIELD_MULT', 'Amount', 'ARGTYPE_IDENTITY', '25');

INSERT INTO PolicyModifiers                                   VALUES('POLICY_FREE_MARKET', 'FREE_MARKET_YIELD_MULT');
INSERT INTO Modifiers         (ModifierID, ModifierType)      VALUES('FREE_MARKET_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('FREE_MARKET_YIELD_MULT', 'YieldType', 'ARGTYPE_IDENTITY', 'YIELD_GOLD');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('FREE_MARKET_YIELD_MULT', 'Amount', 'ARGTYPE_IDENTITY', '25');

INSERT INTO PolicyModifiers                                   VALUES('POLICY_GRAND_OPERA', 'GRAND_OPERA_YIELD_MULT');
INSERT INTO Modifiers         (ModifierID, ModifierType)      VALUES('GRAND_OPERA_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('GRAND_OPERA_YIELD_MULT', 'YieldType', 'ARGTYPE_IDENTITY', 'YIELD_CULTURE');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('GRAND_OPERA_YIELD_MULT', 'Amount', 'ARGTYPE_IDENTITY', '25');

INSERT INTO PolicyModifiers                                   VALUES('POLICY_SIMULTANEUM', 'SIMULTANEUM_YIELD_MULT');
INSERT INTO Modifiers         (ModifierID, ModifierType)      VALUES('SIMULTANEUM_YIELD_MULT', 'MODIFIER_SINGLE_CITY_ADJUST_CITY_YIELD_MODIFIER');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('SIMULTANEUM_YIELD_MULT', 'YieldType', 'ARGTYPE_IDENTITY', 'YIELD_FAITH');
INSERT INTO ModifierArguments (ModifierID, Name, Type, Value) VALUES('SIMULTANEUM_YIELD_MULT', 'Amount', 'ARGTYPE_IDENTITY', '25');
*/