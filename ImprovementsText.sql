INSERT OR REPLACE INTO LocalizedText (Language, Tag, Text)
VALUES
-- Move plant forest from Conservation to Recorded History
("en_US", "LOC_CIVIC_RECORDED_HISTORY_DESCRIPTION", "Allows Builders to plant Woods (second-growth)."),
("en_US", "LOC_CIVIC_CONSERVATION_DESCRIPTION", "Awards 3 [ICON_Envoy] Envoys. Allows the building of National Parks and the purchase of Naturalists with [ICON_Faith] Faith. Woods in your territory that have never been removed (old-growth) gain +1 Appeal. Receive [ICON_TOURISM] Tourism from Walls."),
-- TODO Find out the harvest scaling to include in description. The "increases as the game progresses" I've added is horribly vague.
("en_US", "LOC_FEATURE_FOREST_DESCRIPTION", "Old-growth Woods may be harvested for [ICON_Production] Production. The amount gained increases as the game progresses.[NEWLINE][NEWLINE]Builders may spend a charge to plant a New-growth Woods upon discovering the Recorded History civic."),
("en_US", "LOC_FEATURE_MARSH_DESCRIPTION", "May be harvested for [ICON_FOOD] Food. The amount gained increases as the game progresses."),
("en_US", "LOC_FEATURE_JUNGLE_DESCRIPTION", "May be harvested for [ICON_FOOD] Food and [ICON_Production] Production. The amounts gained increase as the game progresses."),
-- Have camps start as +1 Food, gain +1 Gold at Mercantilism
("en_US", "LOC_IMPROVEMENT_CAMP_DESCRIPTION", "Unlocks the Builder ability to construct Camps.[NEWLINE][NEWLINE]+1 [ICON_Food] Food. Can only be built on valid resources.[NEWLINE][NEWLINE]If built on Luxury resources, the city will gain use of that resource."),
("en_US", "LOC_CIVIC_MERCANTILISM_DESCRIPTION", "Camp improvements receive +1 [ICON_Production] Production and +1 [ICON_Gold] Gold.");