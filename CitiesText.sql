INSERT OR REPLACE INTO LocalizedText (Language, Tag, Text)
VALUES
("en_US", "LOC_LEADER_TRAIT_ZANZIBAR_DESCRIPTION", "Receive the [ICON_RESOURCE_CINNAMON] Cinnamon and [ICON_RESOURCE_CLOVES] Cloves Luxury resources. These cannot be earned any other way in the game, and provide +7 [ICON_Amenities] Amenities each."),
("en_US", "LOC_GREATPERSON_GRANT_PERFUME", "Grants {Amount} [ICON_RESOURCE_PERFUME] Perfume, a uniquely manufactured Luxury resource which provides +7 [ICON_Amenities] Amenities.");