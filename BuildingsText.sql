INSERT OR REPLACE INTO LocalizedText (Language, Tag, Text)
VALUES
-- +25% Science from Campus buildings
("en_US", "LOC_BUILDING_LIBRARY_DESCRIPTION", "+25% [ICON_Science] Science in this city."),
("en_US", "LOC_BUILDING_UNIVERSITY_DESCRIPTION", "+25% [ICON_Science] Science in this city."),
("en_US", "LOC_BUILDING_MADRASA_DESCRIPTION", "A building unique to Arabia. Bonus [ICON_Faith] Faith equal to the adjacency bonus of the Campus district.[NEWLINE][NEWLINE]+25% [ICON_Science] Science in this city."),
("en_US", "LOC_BUILDING_RESEARCH_LAB_DESCRIPTION", "+25% [ICON_Science] Science in this city."),
-- +25% Gold from Commercial Hub buildings
("en_US", "LOC_BUILDING_MARKET_DESCRIPTION", "+25% [ICON_Gold] Gold in this city."),
("en_US", "LOC_BUILDING_SUKIENNICE_DESCRIPTION", "A building unique to Poland. International [ICON_TradeRoute] Trade Routes from this city gain +2 [ICON_Production] Production; domestic [ICON_TradeRoute] Trade Routes gain +4 [ICON_Gold] Gold.[NEWLINE][NEWLINE]+25% [ICON_Gold] Gold in this city."),
("en_US", "LOC_BUILDING_BANK_DESCRIPTION", "+25% [ICON_Gold] Gold in this city."),
("en_US", "LOC_BUILDING_STOCK_EXCHANGE_DESCRIPTION", "+25% [ICON_Gold] Gold in this city."),
-- +25% Production from Industrial Zone buildings
("en_US", "LOC_BUILDING_WORKSHOP_DESCRIPTION", "+25% [ICON_Production] Production in this city."),
("en_US", "LOC_BUILDING_FACTORY_DESCRIPTION", "Bonus is extended to all city centers within 6 tiles. This bonus applies once to a city, and multiple copies of this building within 6 tiles of a city center do not provide additional bonuses.[NEWLINE][NEWLINE]+25% [ICON_Production] Production in this city."),
("en_US", "LOC_BUILDING_ELECTRONICS_FACTORY_DESCRIPTION", "A building unique to Japan. +4 [ICON_Production] Production to all city centers within 6 tiles. After researching the Electricity technology this building provides an additional +4 [ICON_Culture] Culture to its city. This bonus applies once to a city, and multiple copies of this building within 6 tiles of a city center do not provide additional bonuses.[NEWLINE][NEWLINE]+25% [ICON_Production] Production in this city."),
("en_US", "LOC_BUILDING_POWER_PLANT_DESCRIPTION", "Bonus extends to each city center within 6 tiles. This bonus applies once to a city, and multiple copies of this building within 6 tiles of a city center do not provide additional bonuses.[NEWLINE][NEWLINE]+25% [ICON_Production] Production in this city."),
-- +25% Culture from Theater Square buildings
("en_US", "LOC_BUILDING_AMPHITHEATER_DESCRIPTION", "+25% [ICON_Culture] Culture in this city."),
("en_US", "LOC_BUILDING_MUSEUM_ARTIFACT_DESCRIPTION", "Holds [ICON_GreatWork_Artifact] Artifacts. May not be built in a Theater Square district that already has an Art Museum.[NEWLINE][NEWLINE]+25% [ICON_Culture] Culture in this city."),
("en_US", "LOC_BUILDING_MUSEUM_ART_DESCRIPTION", "Holds [ICON_GreatWork_Landscape] Great Works of Art. May not be built in a Theater Square district that already has an Archaeological Museum.[NEWLINE][NEWLINE]+25% [ICON_Culture] Culture in this city."),
("en_US", "LOC_BUILDING_BROADCAST_CENTER_DESCRIPTION", "+25% [ICON_Culture] Culture in this city."),
("en_US", "LOC_BUILDING_FILM_STUDIO_DESCRIPTION", "A building unique to America. +100% [ICON_Tourism] Tourism pressure from this city towards other civilizations in the Modern era.[NEWLINE][NEWLINE]+25% [ICON_Culture] Culture in this city."),
-- +25% Faith from Holy Site buildings
("en_US", "LOC_BUILDING_SHRINE_DESCRIPTION", "Allows the purchasing of Missionaries. Missionaries can only be purchased with [ICON_Faith] Faith.[NEWLINE][NEWLINE]+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_TEMPLE_DESCRIPTION", "Allows the purchasing of Apostles, Gurus, Inquisitors, and with the proper belief, Warrior Monks. These units can only be purchased with [ICON_Faith] Faith.[NEWLINE][NEWLINE]+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_STAVE_CHURCH_DESCRIPTION", "A building unique to Norway. Required to purchase Apostles and Inquisitors with [ICON_Faith] Faith. Holy Site districts get an additional standard adjacency bonus from Woods.  +1 [ICON_PRODUCTION] Production to each coastal resource tile in this city.[NEWLINE][NEWLINE]+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_PRASAT_DESCRIPTION", "A building unique to Khmer. Required to purchase Apostles and Inquisitors with [ICON_Faith] Faith. Missionaries purchased in this city receive the Martyr ability which grants a [ICON_GreatWork_Relic] Relic if this Missionary dies in Theological Combat.[NEWLINE][NEWLINE]+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_CATHEDRAL_DESCRIPTION", "+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_GURDWARA_DESCRIPTION", "+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_MEETING_HOUSE_DESCRIPTION", "+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_MOSQUE_DESCRIPTION", "Missionary and Apostles created here have +1 spread.[NEWLINE][NEWLINE]+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_PAGODA_DESCRIPTION", "+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_SYNAGOGUE_DESCRIPTION", "+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_WAT_DESCRIPTION", "+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_STUPA_DESCRIPTION", "+25% [ICON_Faith] Faith in this city."),
("en_US", "LOC_BUILDING_DAR_E_MEHR_DESCRIPTION", "+1 additional [ICON_FAITH] Faith for each era since constructed or last repaired.[NEWLINE][NEWLINE]+25% [ICON_Faith] Faith in this city."),
-- War Weariness reduction from Encampment buildings: 2% from tier 1, 3% from Armory, 5% from Military Academy
("en_US", "LOC_BUILDING_BARRACKS_DESCRIPTION", "+25% combat experience for all melee and ranged land units trained in this city.[NEWLINE][NEWLINE]Reduces overall War Weariness generated by 2%.[NEWLINE][NEWLINE]May not be built in an Encampment district that already has a Stable."),
("en_US", "LOC_BUILDING_BASILIKOI_PAIDES_DESCRIPTION", "A building unique to Macedon. +25% combat experience for all melee, ranged land units, and Hetairoi trained in this city. Gain [ICON_Science] Science equal to 25% of the unit's cost when a non civilian unit is created in this city.[NEWLINE][NEWLINE]Reduces overall War Weariness generated by 2%.[NEWLINE][NEWLINE]May not be built in an Encampment district that already has a Stable."),
("en_US", "LOC_BUILDING_STABLE_DESCRIPTION", "+25% combat experience for all cavalry class units trained in this city.[NEWLINE][NEWLINE]Reduces overall War Weariness generated by 2%.[NEWLINE][NEWLINE]May not be built in an Encampment district that already has a Barracks."),
("en_US", "LOC_BUILDING_ARMORY_DESCRIPTION", "+25% combat experience for all land units trained in this city.[NEWLINE][NEWLINE]Reduces overall War Weariness generated by 3%."),
("en_US", "LOC_BUILDING_MILITARY_ACADEMY_DESCRIPTION", "+25% combat experience for all land units trained in this city. Allows Corps and Armies to be trained directly. Corps and Army training costs reduced 25%.[NEWLINE][NEWLINE]Reduces overall War Weariness generated by 5%.");
