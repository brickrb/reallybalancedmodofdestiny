-- === Wonders ===

-- Venetian Arsenal - works only for city the wonder is located in (BrickAstley)

INSERT INTO Requirements               (RequirementId, RequirementType)       VALUES ('REQUIRES_CITY_HAS_VENETIAN_ARSENAL', 'REQUIREMENT_CITY_HAS_BUILDING');
INSERT INTO RequirementArguments       (RequirementId, Name, Value)           VALUES ('REQUIRES_CITY_HAS_VENETIAN_ARSENAL', 'BuildingType', 'BUILDING_VENETIAN_ARSENAL');
INSERT INTO RequirementSetRequirements (RequirementSetId, RequirementId)      VALUES ('CITY_HAS_VENETIAN_ARSENAL_REQUIREMENTS', 'REQUIRES_CITY_HAS_VENETIAN_ARSENAL');
INSERT INTO RequirementSets            (RequirementSetId, RequirementSetType) VALUES ('CITY_HAS_VENETIAN_ARSENAL_REQUIREMENTS', 'REQUIREMENTSET_TEST_ALL');
UPDATE      Modifiers                  SET SubjectRequirementSetId = 'CITY_HAS_VENETIAN_ARSENAL_REQUIREMENTS' WHERE ModifierID LIKE 'VENETIAN_ARSENAL%';