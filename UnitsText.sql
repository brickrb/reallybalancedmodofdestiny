INSERT OR REPLACE INTO LocalizedText (Language, Tag, Text)
VALUES
-- Move Corps formation from Nationalism to Guilds, Army formation from Mobilization to Nationalism
("en_US", "LOC_CIVIC_GUILDS_DESCRIPTION", "Allows forming two identical units into a Corps or Fleet."),
("en_US", "LOC_CIVIC_NATIONALISM_DESCRIPTION", "Grants the ability to construct an additional Spy. Allows forming three Identical units into an Army or Armada. Allows one new Casus Belli that can be used to justify wars: Colonial War."),
("en_US", "LOC_CIVIC_MOBILIZATION_DESCRIPTION", "Allows formation of Defensive Pacts. Allows one new Casus Belli that can be used to justify wars: War of Territorial Expansion.");
