I took Crystalline Cat's original RBMod and split it into multiple 
SQL files in order to organize the changes by category. I've provided
proper attribution to BrickAstley and Crystalline Cat in both the SQL files and the 
feature listing for their code.

Mod File List

RBMod-README.txt  :  this file
RBMod-FileListing :  provides a summary of where changes appear in the SQL files
RBMod-FeatureList :  a list of specific changes made 
                     (changes to building and unit costs to numerous to list)
*.sql             :  update files for game databases


Use at your own risk.  Mod is verified to load but changes have not been checked out.

- Suboptimal 26-Jun-2018
