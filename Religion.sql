-- === Religion ===

-- Religious Beliefs (Crystalline Cat)
UPDATE ModifierArguments SET Value = '5' WHERE ModifierId = 'DEFENDER_OF_FAITH_COMBAT_BONUS_MODIFIER' AND Name = 'Amount';

-- All Religions are renameable (Grotsnot)
-- Note: AI players don't know how to use a default name so they will all found no-name religions
UPDATE Religions SET RequiresCustomName = 1 WHERE Pantheon = 0;