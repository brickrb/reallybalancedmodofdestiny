INSERT OR REPLACE INTO LocalizedText (Language, Tag, Text)
VALUES
-- Increase Land Surveyors & Expropriation tile purchase discount from 20% to 33%
("en_US", "LOC_POLICY_LAND_SURVEYORS_DESCRIPTION", "Reduces the cost of purchasing a tile by 33%."),
("en_US", "LOC_POLICY_EXPROPRIATION_DESCRIPTION", "+50% [ICON_Production] Production toward Settlers. Plot purchase cost reduced by 33%."),
-- Increase Ilkum & Public Works builder production modifier from 30% to 50%
("en_US", "LOC_POLICY_ILKUM_DESCRIPTION", "+50% [ICON_Production] Production toward Builders."),
("en_US", "LOC_POLICY_PUBLIC_WORKS_DESCRIPTION", "+50% [ICON_Production] Production toward Builders, and newly trained Builders gain 2 extra build actions."),
-- Reduce various 100% production modifiers to 50%
("en_US", "LOC_POLICY_INTERNATIONAL_WATERS_DESCRIPTION", "+50% [ICON_Production] Production toward Modern, Atomic, and Information era naval units, excluding Carriers."),
("en_US", "LOC_POLICY_LIMES_DESCRIPTION", "+50% [ICON_Production] Production toward defensive buildings."),
("en_US", "LOC_POLICY_MARITIME_INDUSTRIES_DESCRIPTION", "+50% [ICON_Production] Production toward Ancient and Classical era naval units."),
("en_US", "LOC_POLICY_PRESS_GANGS_DESCRIPTION", "+50% [ICON_Production] Production toward Renaissance and Industrial era naval units."),
-- Reduce Professional Army discount from 50% to 33%
("en_US", "LOC_POLICY_PROFESSIONAL_ARMY_DESCRIPTION", "33% discount on all unit upgrades.");