-- === Unit-Related Edits ===

-- Unit-related policies in Policies.sql 

-- Unit Upgrade Formula (Crystalline Cat)
UPDATE GlobalParameters  SET Value =   '0' WHERE Name = 'UPGRADE_BASE_COST';
UPDATE GlobalParameters  SET Value = '100' WHERE Name = 'UPGRADE_NET_PRODUCTION_PERCENT_COST';
  -- This parameter appears to be doubled; previous value was 75.

 
-- Unit Commands (Formation of Corps/Fleets and Armies/Armadas)
UPDATE UnitCommands SET PrereqCivic = 'CIVIC_GUILDS'       WHERE CommandType = 'UNITCOMMAND_FORM_CORPS';
UPDATE UnitCommands SET PrereqCivic = 'CIVIC_NATIONALISM'  WHERE CommandType = 'UNITCOMMAND_FORM_ARMY';
UPDATE Civics SET Description = 'LOC_CIVIC_GUILDS_DESCRIPTION' WHERE CivicType='CIVIC_GUILDS';

-- No more invincible scouts (Grotsnot)
-- Warning: Capturable Archaeologists might have unusual consequences. Have not been able to test stealing one yet.
-- Also note: This will hurt any AI players because they suck at protecting their GPs. Should not be a problem for Realms Beyond games but the effect does exist.
UPDATE Units SET CanRetreatWhenCaptured = 0;


-- Unit Costs (Crystalline Cat)

-- Ranged
-- Slinger, Archer, Pitati Archer, Saka Horse Archer unchanged
UPDATE Units SET Cost = 110 WHERE UnitType = 'UNIT_EGYPTIAN_CHARIOT_ARCHER'; -- Reduced from 120
UPDATE Units SET Cost = 140 WHERE UnitType = 'UNIT_CHINESE_CROUCHING_TIGER'; -- Reduced from 160
UPDATE Units SET Cost = 150 WHERE UnitType = 'UNIT_CROSSBOWMAN'; -- Reduced from 180
UPDATE Units SET Cost = 280 WHERE UnitType = 'UNIT_FIELD_CANNON'; -- Reduced from 330
UPDATE Units SET Cost = 400 WHERE UnitType = 'UNIT_MACHINE_GUN'; -- Reduced from 540

-- Melee
-- Warrior, Eagle Warrior, Immortal, Swordsman, Hypaspist, Legion unchanged
UPDATE Units SET Cost = 100 WHERE UnitType = 'UNIT_KONGO_SHIELD_BEARER'; -- Reduced from 110
UPDATE Units SET Cost = 140 WHERE UnitType = 'UNIT_NORWEGIAN_BERSERKER'; -- Reduced from 180|160
UPDATE Units SET Cost = 140 WHERE UnitType = 'UNIT_JAPANESE_SAMURAI'; -- Reduced from 180|160
UPDATE Units SET Cost = 200 WHERE UnitType = 'UNIT_MUSKETMAN'; -- Reduced from 240
UPDATE Units SET Cost = 200 WHERE UnitType = 'UNIT_SPANISH_CONQUISTADOR'; -- Reduced from 250
UPDATE Units SET Cost = 280 WHERE UnitType = 'UNIT_FRENCH_GARDE_IMPERIALE'; -- Reduced from 340
UPDATE Units SET Cost = 280 WHERE UnitType = 'UNIT_ENGLISH_REDCOAT'; -- Reduced from 340
UPDATE Units SET Cost = 350 WHERE UnitType = 'UNIT_INFANTRY'; -- Reduced from 430
UPDATE Units SET Cost = 350 WHERE UnitType = 'UNIT_DIGGER'; -- Reduced from 430
UPDATE Units SET Cost = 450 WHERE UnitType = 'UNIT_MECHANIZED_INFANTRY'; -- Reduced from 650

-- Anti-Cavalry
-- Spearman, Hoplite unchanged
UPDATE Units SET Cost = 170 WHERE UnitType = 'UNIT_PIKEMAN'; -- Reduced from 200
UPDATE Units SET Cost = 330 WHERE UnitType = 'UNIT_AT_CREW'; -- Reduced from 400
UPDATE Units SET Cost = 420 WHERE UnitType = 'UNIT_MODERN_AT'; -- Reduced from 580

-- Recon
-- Scout unchanged
UPDATE Units SET Cost = 310 WHERE UnitType = 'UNIT_RANGER'; -- Reduced from 380

-- Light Cavalry
-- Horseman unchanged
UPDATE Units SET Cost = 270 WHERE UnitType = 'UNIT_CAVALRY'; -- Reduced from 330
UPDATE Units SET Cost = 280 WHERE UnitType = 'UNIT_RUSSIAN_COSSACK'; -- Reduced from 340
UPDATE Units SET Cost = 430 WHERE UnitType = 'UNIT_HELICOPTER'; -- Reduced from 600

-- Heavy Cavalry
-- Heavy Chariot, War Cart, Hetairoi, Varu unchanged
UPDATE Units SET Cost = 150 WHERE UnitType = 'UNIT_KNIGHT'; -- Reduced from 180
UPDATE Units SET Cost = 150 WHERE UnitType = 'UNIT_ARABIAN_MAMLUK'; -- Reduced from 180
UPDATE Units SET Cost = 200 WHERE UnitType = 'UNIT_POLISH_HUSSAR'; -- Reduced from 250
UPDATE Units SET Cost = 300 WHERE UnitType = 'UNIT_AMERICAN_ROUGH_RIDER'; -- Reduced 385
UPDATE Units SET Cost = 380 WHERE UnitType = 'UNIT_TANK'; -- Reduced from 480
UPDATE Units SET Cost = 470 WHERE UnitType = 'UNIT_MODERN_ARMOR'; -- Reduced from 680

-- Siege
UPDATE Units SET Cost = 110 WHERE UnitType = 'UNIT_CATAPULT'; -- Reduced from 120
UPDATE Units SET Cost = 230 WHERE UnitType = 'UNIT_BOMBARD'; -- Reduced from 280
UPDATE Units SET Cost = 350 WHERE UnitType = 'UNIT_ARTILLERY'; -- Reduced from 430
UPDATE Units SET Cost = 470 WHERE UnitType = 'UNIT_ROCKET_ARTILLERY'; -- Reduced from 680

-- Naval Melee
UPDATE Units SET Cost =  50 WHERE UnitType = 'UNIT_GALLEY'; -- Reduced from 65
UPDATE Units SET Cost =  50 WHERE UnitType = 'UNIT_NORWEGIAN_LONGSHIP'; -- Reduced from 65
UPDATE Units SET Cost = 170 WHERE UnitType = 'UNIT_CARAVEL'; -- Reduced from 240
UPDATE Units SET Cost = 250 WHERE UnitType = 'UNIT_IRONCLAD'; -- Reduced from 380
UPDATE Units SET Cost = 350 WHERE UnitType = 'UNIT_DESTROYER'; -- Reduced from 540

-- Naval Ranged
UPDATE Units SET Cost =  90 WHERE UnitType = 'UNIT_QUADRIREME'; -- Reduced from 120
UPDATE Units SET Cost = 220 WHERE UnitType = 'UNIT_FRIGATE'; -- Reduced from 280
UPDATE Units SET Cost = 325 WHERE UnitType = 'UNIT_BATTLESHIP'; -- Reduced from 430
UPDATE Units SET Cost = 325 WHERE UnitType = 'UNIT_BRAZILIAN_MINAS_GERAES'; -- Reduced from 430
UPDATE Units SET Cost = 470 WHERE UnitType = 'UNIT_MISSILE_CRUISER'; -- Reduced from 680

-- Naval Raider
UPDATE Units SET Cost = 220 WHERE UnitType = 'UNIT_PRIVATEER'; -- Reduced from 280
UPDATE Units SET Cost = 220 WHERE UnitType = 'UNIT_ENGLISH_SEADOG'; -- Reduced from 280
UPDATE Units SET Cost = 350 WHERE UnitType = 'UNIT_SUBMARINE'; -- Reduced from 480
UPDATE Units SET Cost = 315 WHERE UnitType = 'UNIT_GERMAN_UBOAT'; -- Reduced from 430
UPDATE Units SET Cost = 470 WHERE UnitType = 'UNIT_NUCLEAR_SUBMARINE'; -- Reduced from 680
UPDATE Units SET Cost = 350 WHERE UnitType = 'UNIT_AIRCRAFT_CARRIER'; -- Reduced from 540

-- Air
UPDATE Units SET Cost = 300 WHERE UnitType = 'UNIT_BIPLANE'; -- Reduced from 430
UPDATE Units SET Cost = 360 WHERE UnitType = 'UNIT_FIGHTER'; -- Reduced from 520
UPDATE Units SET Cost = 360 WHERE UnitType = 'UNIT_AMERICAN_P51'; -- Reduced from 520
UPDATE Units SET Cost = 425 WHERE UnitType = 'UNIT_JET_FIGHTER'; -- Reduced from 650
UPDATE Units SET Cost = 380 WHERE UnitType = 'UNIT_BOMBER'; -- Reduced from 560
UPDATE Units SET Cost = 480 WHERE UnitType = 'UNIT_JET_BOMBER'; -- Reduced from 700

-- Support
-- Battering Ram, Siege Tower unchanged
UPDATE Units SET Cost = 150 WHERE UnitType = 'UNIT_MILITARY_ENGINEER'; -- Reduced from 170
UPDATE Units SET Cost = 200 WHERE UnitType = 'UNIT_OBSERVATION_BALLOON'; -- Reduced from 240
UPDATE Units SET Cost = 300 WHERE UnitType = 'UNIT_MEDIC'; -- Reduced from 370
UPDATE Units SET Cost = 360 WHERE UnitType = 'UNIT_ANTIAIR_GUN'; -- Reduced from 455
UPDATE Units SET Cost = 420 WHERE UnitType = 'UNIT_MOBILE_SAM'; -- Reduced from 590

-- Miscellaneous Unit Changes (Crystalline Cat)
UPDATE Units SET Range = 2 WHERE UnitType = 'UNIT_MACHINE_GUN';